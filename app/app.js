'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
	'ngRoute',
	'myApp.list',
	'myApp.list.service',
	'myApp.test',
	'myApp.test.mchoice',
	'myApp.focus',
	'myApp.version'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
	$locationProvider.hashPrefix('!');

	$routeProvider.when('/list', {
		templateUrl: 'list/list.html',
		controller: 'ListCtrl'
	});
	$routeProvider.when('/test-rnd/:srclang/:destlang', {
		templateUrl: 'test/single/test.html',
		controller: 'TestCtrl'
	});
	$routeProvider.when('/test-mchoice/:srclang/:destlang', {
		templateUrl: 'test/mc/mchoice.html',
		controller: 'MultipleChoiceCtrl'
	});
	$routeProvider.otherwise({redirectTo: '/list'});
}]);
