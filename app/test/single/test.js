'use strict';

angular.module('myApp.test', ['myApp.list.service'])
	.controller('TestCtrl', ['$scope', '$routeParams', 'listService', function ($scope, $routeParams, listService) {

		var words = listService.getWords(listService.getCurrentListName());
		var srcWords = angular.copy(words);
		var destWords = [];

		var idx = 0;

		var currentWord = '';

		$scope.sourceLang = $routeParams.srclang;
		$scope.destLang = $routeParams.destlang;

		$scope.model = {
			answer: '',
			actualAnswer: ''
		}

		$scope.nextWord = function() {
			if (srcWords.length === 0) {
				srcWords = angular.copy(words);
				destWords = [];
			}

			idx = Math.floor((Math.random() * srcWords.length));
			currentWord = srcWords[idx];

			destWords.push(srcWords[idx])
			srcWords.splice(idx, 1);
			console.debug(destWords.length + ' - ' + srcWords.length);

			$scope.currentWord = currentWord[$scope.sourceLang];
			$scope.model = {
				answer: '',
				actualAnswer: ''
			}
			$scope.answerClass = '';

			focus();
		}

		// validate word
		$scope.checkWord = function () {
			if (angular.uppercase($scope.model.answer) !== angular.uppercase(currentWord[$scope.destLang])) {
				$scope.answerClass = 'incorrect';
				$scope.nrOfIncorrect = $scope.nrOfIncorrect === undefined ? 1 : $scope.nrOfIncorrect + 1;
				focus();
			}
			else {
				$scope.nrOfCorrect = $scope.nrOfCorrect === undefined ? 1 : $scope.nrOfCorrect + 1;
				$scope.answerClass = 'correct';
			}
		},

		$scope.showActualAnswer = function() {
			$scope.model.actualAnswer = currentWord[$scope.destLang];
		}

		var focus = function() {
			$scope.$broadcast('newWord');
		}

		$scope.nextWord();
	}]);