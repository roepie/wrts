'use strict';

angular.module('myApp.test.mchoice', ['myApp.list.service'])
	.controller('MultipleChoiceCtrl', ['$scope', '$routeParams', 'listService', function ($scope, $routeParams, listService) {

		var words = listService.getWords();
		var srcWords = angular.copy(words);
		var destWords = [];

		var idx = 0;

		var currentWord = '';

		$scope.sourceLang = $routeParams.srclang;
		$scope.destLang = $routeParams.destlang;

		$scope.model = {
			answer: ''
		}

		$scope.nextWord = function () {
			if (srcWords.length === 0) {
				srcWords = angular.copy(words);
				destWords = [];
			}

			idx = Math.floor((Math.random() * srcWords.length));
			currentWord = angular.copy(srcWords[idx]);


			$scope.currentWord = currentWord[$scope.sourceLang];
			$scope.model.answer = '';

			destWords.push(currentWord)
			srcWords.splice(idx, 1); // remove from src array
			console.debug(srcWords.length + ' - ' + destWords.length + ' - ' + words.length);
			console.debug(currentWord);

			$scope.answerClass = '';
			setChoices(_getOriginalIndex(currentWord));

			focus();
		}

		// validate word
		$scope.checkWord = function () {
			if ($scope.model.answer !== currentWord[$scope.destLang]) {
				$scope.answerClass = 'incorrect';
				$scope.nrOfIncorrect = $scope.nrOfIncorrect === undefined ? 1 : $scope.nrOfIncorrect + 1;
				focus();
			}
			else {
				$scope.answerClass = 'correct';
				$scope.nrOfCorrect = $scope.nrOfCorrect === undefined ? 1 : $scope.nrOfCorrect + 1;
			}
		}

		/**
		 * Randomize array element order in-place.
		 * Using Durstenfeld shuffle algorithm.
		 */
		function shuffle(array) {
			for (var i = array.length - 1; i > 0; i--) {
				var j = Math.floor(Math.random() * (i + 1));
				var temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			return array;
		}

		var setChoices = function (currentIdx) {
			var idxs = [currentIdx];
			for (var i = 0; i < words.length; i++) {
				var extraIdx = Math.floor((Math.random() * words.length));
				if (idxs.indexOf(extraIdx) == -1) {
					idxs.push(extraIdx);
				}
				if (idxs.length == 4) {
					break;
				}
			}

			shuffle(idxs);

			$scope.choices = [
				words[idxs[0]][$scope.destLang], words[idxs[1]][$scope.destLang], words[idxs[2]][$scope.destLang], words[idxs[3]][$scope.destLang]
			];
		}

		var focus = function () {
			$scope.$broadcast('newWord');
		}

		var _getOriginalIndex = function(currentWord) {
			for (var i = 0; i < words.length; i++) {
				if (words[i].en === currentWord.en && words[i].nl === currentWord.nl) {
					return i;
				}
			}
			return;
		}

		$scope.nextWord();
	}]);