'use strict';

angular.module('myApp.list', ['myApp.list.service'])
	.controller('ListCtrl', ['$scope', 'listService', function ($scope, listService) {

		$scope.model = {
			listName: '',
			dutch: '',
			english: ''
		}

		$scope.deleteEnabled = false;
		$scope.currentListName = listService.getCurrentListName();
		$scope.words = listService.getWords($scope.currentListName);
		$scope.listNames = listService.getListNames();

		$scope.addWord = function () {
			// both fields need to be filled in
			if ($scope.model.dutch === '' || $scope.model.english === '') {
				return;
			}

			listService.addWord($scope.model.english, $scope.model.dutch);
			$scope.words = listService.getWords();
			$scope.model = {
				dutch: '',
				english: ''
			}
		}

		$scope.removeWord = function (item) {
			listService.removeWord(item);
			$scope.words = listService.getWords();
			$scope.model = {
				dutch: '',
				english: ''
			}
			focus();
		}

		$scope.toggleRemove = function () {
			$scope.deleteEnabled = !$scope.deleteEnabled;
		}

		$scope.loadList = function (listName) {
			listService.setCurrentListName(listName);
			$scope.words = listService.getWords();
			$scope.currentListName = listName;
		}

		$scope.createList = function () {
			listService.createList($scope.model.listName);
			listService.setCurrentListName($scope.model.listName);
			$scope.words = listService.getWords();
			$scope.currentListName = $scope.model.listName;
			$scope.listNames = listService.getListNames();
		}

		var focus = function () {
			$scope.$broadcast('newWord');
		}

	}]);