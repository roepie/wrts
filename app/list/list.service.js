'use strict';

angular.module('myApp.list.service', [])
	.service('listService', ['$http', function ($http) {

		/**
		 * constant name of the holder of the currently active list
		 * @type {string}
		 */
		const CURRENT_LIST_NAME = 'currentListName';

		return {
			getWords: getWords,
			addWord: addWord,
			removeWord:removeWord,
			getListNames: getListNames,
			getCurrentListName:getCurrentListName,
			setCurrentListName:setCurrentListName,
			createList:createList,
			getList:getList
		}

		function createList(listName) {
			localStorage.setItem(listName, JSON.stringify({created:new Date().getTime(),words:[]}));
		}

		/**
		 * retrieves the name of the currently active list of words
		 */
		function getCurrentListName() {
			return localStorage.getItem(CURRENT_LIST_NAME);
		}

		/**
		 * saves the currently active list of words
		 * @param listName the name of the list
		 */
		function setCurrentListName(listName) {
			return localStorage.setItem(CURRENT_LIST_NAME, listName);
		}

		/**
		 * retrieve the words for the currently active list
		 * @returns {Array}
		 */
		function getWords() {
			if (localStorage.getItem(getCurrentListName()) === undefined) {
				return [];
			}
			return JSON.parse(localStorage.getItem(getCurrentListName())).words;
		}

		/**
		 * retrieves the whole list, including the metadata
		 */
		function getList() {
			return JSON.parse(localStorage.getItem(getCurrentListName()));
		}

		/**
		 * add a word to the currently active list
		 * @param enVal the word in english
		 * @param nlVal the word in dutch
		 */
		function addWord(enVal, nlVal) {
			var list = getList();
			list.words.push({created:new Date().getTime(),nl:nlVal, en:enVal});
			localStorage.setItem(getCurrentListName(), JSON.stringify(list));
		}

		/**
		 * removes a word by index from the currently active list
		 * @param idx the index
		 */
		function removeWord(word) {
			var list = getList();
			var listWord = _getListWord(list, word);
			list.words.splice(list.words.indexOf(listWord), 1);
			localStorage.setItem(getCurrentListName(), JSON.stringify(list));
		}

		function _getListWord(list, word) {
			var listWord = '';
			angular.forEach(list.words, function(_word) {
				if (_word.en === word.en && _word.nl === word.nl) {
					listWord = _word;
				}
			});
			return listWord;
		}

		/**
		 * retrieves the names of all the word lists
		 * @returns {Array}
		 */
		function getListNames() {
			var listNames = [];
			for ( var i = 0, len = localStorage.length; i < len; ++i ) {
				if (localStorage.key(i) !== CURRENT_LIST_NAME) {
					listNames.push(localStorage.key(i));
				}
			}
			return listNames;
		}

	}]);

